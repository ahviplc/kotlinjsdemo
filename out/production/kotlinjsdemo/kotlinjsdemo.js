if (typeof kotlin === 'undefined') {
  throw new Error("Error loading module 'kotlinjsdemo'. Its dependency 'kotlin' was not found. Please, check whether 'kotlin' is loaded prior to 'kotlinjsdemo'.");
}
var kotlinjsdemo = function (_, Kotlin) {
  'use strict';
  var println = Kotlin.kotlin.io.println_s8jyv4$;
  var throwCCE = Kotlin.throwCCE;
  var trim = Kotlin.kotlin.text.trim_gw00vp$;
  function main(args) {
    var message = 'Hello JavaScript! By LC';
    println(message);
    var msg = 'Hello World!';
    println(msg);
    sayTime();
  }
  function getEmail() {
    return document.getElementById('email');
  }
  function sayTime() {
    println(new Date());
  }
  function max(a, b) {
    return a > b ? a : b;
  }
  function min(a, b) {
    return a < b ? a : b;
  }
  function substring(src, start, end) {
    return src.substring(start, end);
  }
  function trim_0(src) {
    var tmp$;
    return trim(Kotlin.isCharSequence(tmp$ = src) ? tmp$ : throwCCE()).toString();
  }
  _.main_kand9s$ = main;
  _.getEmail = getEmail;
  _.sayTime = sayTime;
  _.max_vux9f0$ = max;
  _.min_vux9f0$ = min;
  _.substring_3m52m6$ = substring;
  _.trim_61zpoe$ = trim_0;
  main([]);
  Kotlin.defineModule('kotlinjsdemo', _);
  return _;
}(typeof kotlinjsdemo === 'undefined' ? {} : kotlinjsdemo, kotlin);
