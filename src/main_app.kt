import org.w3c.dom.Element
import kotlin.browser.document
import kotlin.js.Date

fun main(args: Array<String>) {
    val message = "Hello JavaScript! By LC"
    println(message)
//    for (i in 1..10)
//        println(i)
    val msg = "Hello World!"
    println(msg)
//    js("console.log(msg)")
//    js("alert(msg)")
//    js("alert('KotlinJS:'+new Date())")
    //js("sayTime()")
    sayTime()
//    val emailElement = getEmail()
//    println(emailElement?.getAttribute("value"))
}

fun getEmail(): Element? {
    return document.getElementById("email")
}

fun sayTime() {
    println(Date())
}

fun max(a: Int, b: Int): Int {
    return if (a > b) a else b
}

fun min(a: Int, b: Int): Int {
    return if (a < b) a else b
}

fun substring(src: String, start: Int, end: Int): String {
    return src.substring(start, end)
}

fun trim(src: String): String {
    return src.trim()
}